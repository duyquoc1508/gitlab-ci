const request = require('supertest');
const app = require('../index');

describe('GET /', function () {
  it('Response with true data', function (done) {
    request(app).get('/').expect(
      200,
      {
        message: 'Hello GitLab-CI!'
      },
      done
    );
  });
});
