[![pipeline status](https://gitlab.com/duyquoc1508/gitlab-ci/badges/main/pipeline.svg)](https://gitlab.com/duyquoc1508/gitlab-ci/-/commits/main)

## Giải thích Pipeline cho project

Git branches:

- **main**: dùng để deploy staging và production
- **dev**: dùng để deploy dev

Environments:

1. Deploy dev: Tự động deploy khi nhánh `dev` có thay đổi

2. Deploy staging: Tự động deploy khi trên nhánh `main` có sự thay đổi (CI_DEFAULT_BRANCH || CI_COMMIT_REF_PROTECTED ...)

3. Deploy production: Là job sau khi deploy staging thành công và phải thực hiện thủ công

## Setup CI/CD pipeline for NodeJS app with Gitlab-CI

### Workflow

- Commit NodeJS application to GitLab
- Build Docker Image for NodeJS application
- Push the Docker Image to GitLab Container registry as Test image
- Run Test on NodeJS application
- Tag the test image as release image push to GitLab Container registry
- Deploy the application as Docker container using release image

![](images/workflow.png 'workflow')

Run a gitlab runner with docker

- First create an instance of the GitLab Runner container

```sh
docker run -d --name gitlab-runner --restart always -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:v14.7.0
```

Khi dùng executor là shell thì docker sẽ không có trong gitlab-runner container. bởi vì docker không được install trong gitlab runner image, lúc này khi chạy các script docker trong gitlab ci sẽ bị lỗi `docker: command not found`. Cách giải quyết là mount volume từ host vào gitlab runner (thêm đoạn sau vào lên docker run `-v /usr/bin/docker:/usr/bin/docker`)

- Vào gitlab runner container và register runner

```sh
# Command này áp dụng cho gitlab runner được cài bằng docker
docker exec -it gitlab-runner gitlab-runner register [--docker-privileged]
# hoặc config 1 bằng 1 dòng duy nhất
docker exec -it gitlab-runner gitlab-runner register -n --url GITLAB_URL --registration-token REGISTRATION_TOKEN --executor docker --description "My Docker Runner" --docker-image "docker:19.03.12" --docker-volumes /var/run/docker.sock:/var/run/docker.sock --tag-list "docker_executor"
```

Trong dấu ngoặc [] là có thể có hoặc không. Có enable chế độ privileged của docker hay không. Khi job đang chạy docker:dind thì phải enable cờ này

Sau khi register xong mà thấy gitlab-runner trên gitlab chưa active thì dùng lệnh dưới để start thủ công cho runner

- start

```
docker exec -it gitlab-runner gitlab-runner start
```

Trong môi trường docker:19 mà job chúng ta đang chạy, theo lý thuyết nó có docker, nhưng để chạy được docker command trong đó thì ta cần 1 container để support đó là `docker:dind` đóng vai trò `docker deamon` (Docker Server), còn `docker:19` phía ngoài là docker client. (`docker-cli` hay còn gọi là Docker Client là thứ mà ta chạy ở command line `docker build`...)

> Docker Daemon (Docker server) là thứ quản lý tất cả mọi thứ liên quan tới Docker: images, containers, networks,...

### Tại sao phải cần docker:dind

Vấn đề được mô tả như sau

![](images/docker-only.png 'docker only')

- Đầu tiên runner sẽ pull image `docker:19` về chạy lên để tạo môi trường để chạy job của chúng ta ở trong đó

- Nhưng ở trong cái môi trường "đó" - môi trường bên trong `docker:19` thì mặc định sẽ không kết nối với `docker deamon` được và phải cần đến sự trợ giúp của `docker:din`. (Chạy docker bên trong docker image, docker image ở đây là docker:19)

![](images/docker-in-docker.png 'docker in docker')

The `docker:dind` service is a deamon this is created **just** for your job. This is incredibly important because it can prevent concurrent jobs from stepping on one another or being able to escalate access where they might not otherwise have it.

When the build starts, the Gitlab runner will create 2 containers. container to run `job` and `docker:dind` container, they `linked` together. When job invokes `docker` command, job will connect to the `docker:dind` container, which then carries out the requested commands

Any containers created by job (by invokes `docker run`, `docker build`... as a part of your job) are managed by the docker deamon running on `docker:din` container, not the host deamon. If we run `docker ps` inside job, we'll notice that none of the containers run on the host deamon are listed, despite the fact that if we run `docker ps` on the host, we would see the job container, the dind container...

Vậy giờ ta chỉ cần cài Docker vào trong image `docker:19` là được. Để hỗ trợ ta không phải cài bằng tay thì gitlab hổ trợ chỉ cần thêm `docker:dind` vào service là được 2 images `docker:19` và `docker:dind` sẽ "link" với nhau và ta có thể chạy được command `docker build ...` như bình thường

- Why `docker` need `docker:dind` as a service?

> Docker uses a client-server architecture. The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers. The Docker client and daemon can run on the same system, or you can connect a Docker client to a remote Docker daemon. The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface.

Locally, we don’t really think about this as both the client and the daemon are installed on the `same machine`. But in GitLab things are a bit different.

So in GitLab, the `docker` image is simply the `client`. The `docker:dind` image is the `daemon` and it is started as a service, offering network-accessible services. The client can communicate with the daemon through the network interface

- How can access application in docker:dind

The Node.js application exposing an API on port 3000 has been started by the Docker Daemon, within the `docker:dind` container

So using `localhost` will not work, as the API is running on a different container. Fortunately, GitLab has auto-generated a hostname for the respective service. In this case, the hostname is `docker`

So we can access the application with cURL by adapting the command to use `docker` instead of `localhost`

## Các cách để sử dụng docker command trong CI/CD jobs

- Shell executor
  Cách này phải cấp quyền cho `gitlab-runner` user vào group `docker`
- Docker executor with docker image (Docker-in-docker)
  Cách này sử dụng docker-in-docker, liên quan đến lỗi TLS và DOCKER_HOST
- Docker socket binding
  Cách này sẽ **bind-mount /var/run/docker.sock** vào docker executor bên trong gitlab runner (mount ở bên trong config.toml của gitlab runner `volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]`). Docker bây giờ sẽ available trong context của image. Máy cài đặt gitlab runner phải được cài đặt docker

## Note

- Nếu sử dụng gitlab runner của mình (Gitlab runner được cài đặt trên server deploy app) để thực hiện các job như build, test, mà sau đó không cần giữ application running thì sử dụng `docker:dind`. Tuy nhiên, với `docker:dind` này sau khi gitlab ci complete job thì nó sẽ tự động dọn dẹp các container sinh ra trong quá trình job execute, vì vậy nên application của chúng ta được chạy trong `docker:dind` cũng sẽ bị remove theo

- Khi pipeline mà có đòi hỏi app của chúng ta vẫn phải running sau khi quá trình ci kết thúc (job complete). Giống như deploy server, sau khi deploy thì application phải running. Vậy nên không thể dùng docker:dind dùng service `docker:dind`

- Gitlab runner được cài đặt bằng docker hay cài trực tiếp vào máy vật lý thì các config sau đó vẫn không khác nhau gì

# Gitlab runner on windows

Cài đặt như hướng dẫn
https://docs.gitlab.com/runner/install/windows.html

registration > install > start

Sau khi register runner xong, thay đổi file `config.toml`

```
privileged = true # nếu job này có sử dụng docker:dind
```

Sau đó restart lại runner

```shell
.\gitlab-runner.exe restart
```

## Troubleshooting

Cannot connect to the Docker daemon at tcp://docker:2375. Is the docker daemon running?
-> Là do docker client không connect được tới docker deamon. Fix bằng docker:dind hoặc mount docker.sock từ host (nơi cài gitlab runner) vào docker executor trong gitlab runner tùy vào cách job thực hiện

Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock
-> Lỗi này có thể gặp đối với executor là `shell`, shell executor chưa có quyền truy cập docker ở host nơi cài đặt gitlab-runner

### Reference

- [Install GitLab Runner with Docker](https://datawookie.dev/blog/2021/03/install-gitlab-runner-with-docker/)
- [Nhập môn CICD với Gitlab](https://viblo.asia/p/nhap-mon-cicd-voi-gitlab-07LKX9WPZV4)
- [How to Start a Docker Container Inside your GitLab CI pipeline](https://medium.com/devops-with-valentine/how-to-start-a-docker-container-inside-your-gitlab-ci-pipeline-bfeb610c3f4)
- [docker executor vs docker dind image](https://stackoverflow.com/questions/71416920/docker-executor-vs-docker-dind-image)
- [Enable Docker commands in your CI/CD jobs 3 ways](https://microfluidics.utoronto.ca/gitlab/help/ci/docker/using_docker_build.md#use-docker-socket-binding). (Đầy đủ nhất)
