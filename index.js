const express = require('express');
const app = express();

const HOST = '0.0.0.0';
const PORT = 3000;

app.get('/', (req, res) => {
  res.json({
    message: 'Hello GitLab-CI!'
  }).status(200);
})

app.get('/info', (req, res) => {
  res.json({
    message: 'Welcome to tutorial setup CI/CD pipeline for NodeJS app with Gitlab-CI'
  }).status(200);
})

app.listen(PORT, HOST, () => {
  console.log(`Server running at ${HOST}:${PORT}`);
})

module.exports = app;
